# 0x0Uploader

Uploader for the 0x0 file sharing service.

## Requirements

- bash
- curl

### Optional

- xclip
- wl-clipboard
- libnotify

## How to use

- Download
- Make file executable
- Set "Open with" options, for filetypes
- Modify Instance file for alternative server
- From terminal, use 0x0uploader file

## Donation

If you like my work, feel free to donate: https://liberapay.com/MorsMortium/